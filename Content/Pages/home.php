<section>
  <?php //include ("Views/Shared/Partials/feature-projects-carousel.php"); ?>
</section>

<section class="project-categories">
  <div class="container">
    <div class="row">

      <div class="col-md project-category oos-time">
        <h2>Out-of-School Time</h2>

        <ul>
          <li>
            <a href="tfcclc-overview">21st Century Community Learning Centers <small>(21st CCLC)</small></a>
          </li>

          <li>
            <a href="yds-overview">Youth Driven Spaces</a>
          </li>

          <li>
            <a href="rubric-overview">Youth-Adult Partnership RUBRIC</a>
          </li>
        </ul>
      </div>

      <div class="col-md project-category ece">
        <h2>Early Childhood Education</h2>

        <ul>
          <li>
            <a href="gsrp-overview">Great Start Readiness Program (GSRP)</a>
          </li>

          <li>
            <a href="sbp-overview">Strong Beginnings Program</a>
          </li>

          <li>
            <a href="cmp-overview">Child Care Mapping Project</a>
          </li>
        </ul>
      </div>

    </div>
  </div>
</section>



<section class="mt-60">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="d-inline">Statewide Evaluation</h2>
                <a href="projects" class="ml-3 text-muted small">View all Projects</a>
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 projects-list">
                <?php include 'Views/Shared/Partials/projects-list.php'; ?>
            </div>
        </div>

        </div>



    </div>
</section>


<section class="intro">
  <div class="container">
      <div class="row">
        <div class="intro-text col-md-7 align-self-center pb-5 pb-lg-0 pr-sm-5 order-md-1">

          <h2> About Us </h2>
          <p>
            The Community Evaluation Programs group conducts research and evaluation projects focusing on the quality, accessibility and impacts of out-of-school time and preschool education across the state of Michigan. We have years of experience working with communities to produce engaged scholarship and to promote a more diverse, equitable and inclusive environment for all.
          </p>

          <a href="about" class="btn btn-theme btn-theme-primary" id="about-link">
              Learn more
            </a>
        </div>

        <div class="intro-photo col-md-5 order-md-0">
          <div class="row">
            <div class="col-md-11">
              <img src="Content/Images/about-cep.jpg" class="img-fluid" alt="" />
            </div>
          </div>
        </div>
      </div>
  </div>
</section>