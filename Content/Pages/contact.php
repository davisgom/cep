<h1>
  <?php echo str_replace("-", " ", ucfirst($page_content)); ?>
</h1>

<p>For questions and information, please contact us at:</p>

<hr />
<div itemscope="" itemtype="http://schema.org/EducationalOrganization">
<div itemprop="department"><strong>Community Evaluation Programs</strong></div>

<div class="parentOrganization">Michigan State University</div>

<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
<div itemprop="streetAddress">219 S. Harrison Road, Room 93</div>
<span itemprop="addressLocality">East Lansing</span>, <span itemprop="addressRegion"><abbr title="Michigan">MI</abbr></span> <span itemprop="postalCode">48824</span></div>

<div class="tel">Telephone:<span itemprop="telephone"> (517) 353-8977 </span></div>

<div class="fax">Fax:<span itemprop="faxNumber"> (517) 432-9541 </span></div>

<div class="email">E-mail: <a href="mailto:cerc@msu.edu">cerc@msu.edu</a></div>
</div>
