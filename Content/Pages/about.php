<h1>
  <?php echo str_replace("-", " ", ucfirst($page_content)); ?>
</h1>

<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<br />

<section class="theme-callout">
  <div class="container">
    <div class="row text-white">
      <div class="col">

        <h2 class="ssp">
          Office for Public Engagement and Scholarship
        </h2>

        <hr/>

        <p>
          The Office for Public Engagement and Scholarship (OPES) advances MSU's engagement mission by supporting faculty, staff, and students in their community-engaged research and creative activities, teaching, and service. OPES also contributes to the University's reputation as a national and international leader in community engagement by ensuring that MSU is represented in critical organizations, contributing to the literature, and through other scholarly activities.
        </p>

        <a href="https://engage.msu.edu/about/departments/office-for-public-engagement-and-scholarship" class="btn btn-theme btn-theme-secondary" <?php echo $external; ?>>
            Learn more
          </a>
      </div>
    </div>
  </div>
</section>
