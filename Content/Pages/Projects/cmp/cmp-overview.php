<?php
$page_title = "Overview";
?>

<div  class="feature-image">
  <img src="Content/Images/mapping.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<p>
  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
</p>

<br  />

<div class="card">
  <div class="card-header">
    <h2>People:</h2>
  </div>

  <div class="card-body">
    <p class="w-75">
      <strong class="card-person">
        <a href="#">Jamie Heng-Chieh Wu</a> <br/>
      </strong>
      <strong>Principal Investigator</strong>, 21st CCLC State Evaluation, GSRP State Evaluation, Expanding Youth-Driven Space Across Southeast Michigan <br />
      <strong>Associate Director for Community Evaluation Programs,</strong> Office for Public Engagement and Scholarship
    </p>

    <hr />

    <p class="w-75">
      <strong class="card-person">
        <a href="#">Laurie A. Van Egeren</a> <br/>
      </strong>
      <strong>Principal Investigator</strong>, 21st CCLC State Evaluation<br />
      <strong>Interim Associate Provost for University Outreach and Engagement</strong>
      <strong>Adjunct Faculty</strong>, Department of Human Development and Family Studies
    </p>
  </div>
</div>
