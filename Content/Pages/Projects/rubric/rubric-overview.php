<?php
$page_title = "Overview";
?>

<div  class="feature-image">
  <img src="Content/Images/rubric.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  Funded by the Edmund A. Stanley Jr. research grant from the <a href="http://www.robertbownefoundation.org/" <?php echo $external; ?>>Robert Bowne Foundation</a>, this Rubric is developed through a research study consisting of extensive literature view, a series of program observations, youth and adult interviews and focus groups to assess the practices of Youth-Adult Partnership (Y-AP) in various youth settings (i.e., school reforms, afterschool programs, camps, parks and recreation, youth participatory action research, etc.). Similar to the concepts of "youth-driven" and "youth civic engagement," the Y-AP approach aims to increase <strong>youth voice</strong> and <strong>youth leadership</strong> in affairs that affect them and their communities.
</p>

<p>
  The Rubric was conducted in partnership with <a href="http://neutral-zone.org/" <?php echo $external; ?>>The Neutral Zone</a>, Ann Arbor's teen center (where teens lead, create and innovate). It follows the framework of the article "<strong>The Psychology and Practice of Youth-Adult Partnership</strong>" (Zeldin, Christens, & Powers, 2013) to capture specific behaviors and social climates supporting the four critical dimensions of Y-AP:
</p>

<ol>
  <li><strong>authentic decision-making</strong></li>
  <li><strong>natural mentors</strong></li>
  <li><strong>reciprocity</strong></li>
  <li><strong>community connectedness</strong></li>
</ol>

<p>
  Our goal is to provide a freely available assessment tool for professional development and program evaluation. You're encouraged to <a href="#">download the fillable form</a> above and hit the "submit" button in the PDF file when you're done. Contact Dr. Jamie Wu at <a href="#">wuhengch@msu.edu</a> or 517-884-1412 for more information.
</p>
