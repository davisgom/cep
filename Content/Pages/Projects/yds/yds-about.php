<?php
$page_title = "About";
?>


<h2>The University-Community Partnership</h2>

<p>Since 2010, MSU&rsquo;s Community Evaluation and Research Collaborative (CERC) at University Outreach and Engagement has established an extensive and mutually beneficial partnership with Neutral Zone (NZ), with a common goal to empower youth in communities across Michigan.</p>

<ul>
	<li><strong>2010-2012: YDS Pilot Study</strong><br />
	Funded by the Kellogg Foundation, CERC evaluated the process through which YDS practices were disseminated in eight youth-serving agencies across Michigan. Using data collected from surveys, interviews, focus groups, and coaching logs, CERC was able to demonstrate the impacts of the YDS intervention, identify YDS core elements, and document how the practices can be utilized in different types of youth settings.</li>
	<li><strong>2013-2014: Development of the <a href="#">Youth-Adult Partnership Rubric</a> </strong><br />
	Based on its field experiences with the YDS pilot study, CERC was awarded a research grant through the National Institute on Out-of-School Time to help identify quality standards and model practices on facilitating youth-adult partnerships. The rubric follows a conceptual framework developed by Zeldin, Christens, &amp; Powers (2013) and can be used for evaluation and professional development trainings.</li>
	<li><strong>2018-present: Expanding YDS in Southeast Michigan </strong><br />
	CERC&rsquo;s objective for this current initiative is twofold: (1) to support NZ and the participating agencies with timely feedback so coaching and training efforts can be best utilized, and (2) to identify best practices and effective ways of enhancing youths&rsquo; experiences in meaningful decision-making, identifying mentors, co-learning with adults, and feeling connected to their organizations and the community at-large. These experiences represent the four key aspects of youth-adult partnerships, and are aligned with high quality program standards. Working together with NZ, our goal is to transform these organizations into YDS model sites where teens are able to find a collective voice within themselves, their community, and local organizations.</li>
	<li><a href="https://cerc.msu.edu/upload/yds/Expanding YDS across Southeast Michigan_Year 1 Report-edited.pdf">2019: Year 1 Report</a></li>
	<li><a href="https://cerc.msu.edu/upload/yds/Expanding Youth-Driven space Across Southeast Michigan 2019-20-acc.pdf">2020: Year 2 Report</a></li>
	<li><a href="https://cerc.msu.edu/upload/yds/YDS Year 3 Report_Final.pdf">2021: Year 3 Report</a></li>
</ul>
