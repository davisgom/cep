<?php
$page_title = "Overview";
?>

<div  class="feature-image">
  <img src="Content/Images/yds.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  Started in the summer of 2018, the Evaluation of Expanding Youth-Driven Space Across Southeast Michigan is a three-year project funded by the <a href="http://www.ralphcwilsonjrfoundation.org">Ralph C. Wilson, Jr. Foundation</a>. The Youth-Driven Space (YDS) refers to a positive youth development environment where youth partner with adults to meaningfully engage in decision-making throughout the organization&rsquo;s governance and programming activities.
</p>

<p>
  Led by the <a href="https://www.neutral-zone.org">Neutral Zone</a>, youth-serving agencies participating in this initiative will receive intensive coaching and training supports to promote youth leadership, social and emotional learning, and effective school and community change. The first cohort includes six diverse youth-serving agencies: <a href="https://www.accesscommunity.org">ACCESS</a>, <a href="http://knowresolve.org">KnowResolve</a>, <a href="https://givemerit.org">Give Merit</a>, <a href="https://www.ward1productions.com/">WARD 1 Productions</a>, <a href="http://cocswdetroit.com">Congress of Communities</a>, and <a href="https://www.482forward.org">482Forward</a>.
</p>

<p>YDS involves three core practices:</p>

<ol>
	<li>Tapping teens&rsquo; intrinsic motivation</li>
	<li>Supporting teens&rsquo; developmental needs</li>
	<li>Fostering genuine partnerships between adults and youth</li>
</ol>
