<?php
$page_title = "Overview";
?>

<div  class="feature-image">
  <img src="Content/Images/gsrp.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  Mchigan's Great Start Readiness Program (GSRP) is a state-funded preschool program for four-year-old children with factors that place them at risk for educational failure. The program provides high-quality academic and social-emotional experiences that prepare Michigan children for kindergarten.
</p>

<p>
  <a href="gsrp-team">Our team of researchers at Michigan State University</a> is conducting a longitudinal evaluation of the GSRP.
</p>

<p class="mb-0">
  The goals are to:
</p>

<ul>
	<li>Support data-informed decision making</li>
	<li>Provide scientific evidence of the impacts of the state&rsquo;s investment in GSRP</li>
</ul>

<p>
  The maps on these pages provide detailed snapshots of GSRP for four-year-old children. Policymakers, program administrators, and other stakeholders can use these data to drive policy decisions and improve program quality.
</p>

<p>
  In the 2020-2021 program year (the fourth year of the evaluation), GSRP funding was awarded to 56 independent school districts (ISDs): 52 ISDs and 2 consortia representing 4 ISDs. The ISDs oversaw subrecipients managing 1,192 sites and 2,287 classrooms. GSRP classrooms were available in 473 Michigan school districts. Of the 28,422 children served, 89% came from low-income families. The racial/ethnic breakdown is:
</p>

<ul>
	<li>57% White (non-Hispanic)</li>
	<li>24% Black</li>
	<li>11% Hispanic/Latino</li>
	<li>5% multiracial</li>
	<li>2% Asian</li>
	<li>1% American Indian/Alaska Native</li>
	<li>less than 1% Hawaiian/Pacific Islander</li>
</ul>

<div class="card mt-4 small">
  <div class="card-body">
    <p class="mb-0 text-muted">Data Source: 2020-21</p>
  </div>
</div>
