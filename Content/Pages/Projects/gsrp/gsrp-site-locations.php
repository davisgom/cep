<?php
$page_title = "ISD and Site Locations";
?>

<p>
  This map shows all 1192 GSRP sites in the 2020-2021 school year, differentiated by whether the sites are based in schools or in community-based organizations (CBOs).
</p>

<ul>
 <li>Hover over a site dot to see details about that site.</li>
 <li>Hover over the ISD boundary to see statistics about the GSRP program in that ISD.</li>
 <li>Use the buttons on the left of the map to zoom in or out.</li>
 <li>Use the buttons at the bottom of the map to share, download, or expand to full screen.</li>
</ul>

<script type='text/javascript' src='https://vis.itservices.msu.edu/javascripts/api/viz_v1.js'></script>
<div class="tableauPlaceholder" style="width: 100%; overflow: scroll;">
  <object class="tableauViz" height="827" style="display:none;" width="100%">
    <param name="host_url" value="https%3A%2F%2Fvis.itservices.msu.edu%2F" />
    <param name="embed_code_version" value="3" /><param name="site_root" value="/t/GSRPEvaluation" />
    <param name="name" value="SiteLocationsWithZIPandOperationFilters/SiteLocationsv2" />
    <param name="tabs" value="no" />
    <param name="toolbar" value="yes" />
    <param name="showAppBanner" value="false" />
  </object>
</div>
