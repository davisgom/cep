<?php
$page_title = "MSU Team";
?>

<p>
  Our team consists of collaborators from MSU's interdisciplinary and cross-institutional backgrounds with experience working on multi-site evaluations in K-12 and early childhood program settings. All the senior personnel are well-experienced with large-scale data collection, management, and analysis, and are expected to widely disseminate the findings through the affiliated networks across the fields of evaluation, education, child development, economy, social policies, and statistics.
</p>

<div class="card mt-5">
  <div class="card-body">

    <div>
      <p>
        <strong class="card-person">
          <a href="#">Jamie Heng-Chieh Wu</a> <br/>
        </strong>
        <strong>Principal Investigator</strong>, Recreation and Youth Development <br />
        <em class="small d-inline-block w-75">
          K-12 out-of-school time, program evaluation, hierarchical linear modeling, Co-PI for the Michigan 21st Century Community Learning Centers (CCLC) statewide evaluation project
        </em>
      </p>

      <p>
        Dr. Wu has had more than a decade of experience conducting K-12 out-of-school time program evaluation and was named as one of the Most Influential in Research and Evaluation 2018 by the National AfterSchool Association.
      </p>
    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Laurie Van Egeren</a> <br/>
        </strong>
        <strong>Co-Principal Investigator</strong>, Developmental Psychology <br />
        <em class="small d-inline-block w-75">
          Child development, cluster randomized trial, Co-PI for the Michigan 21st CCLC statewide evaluation, PI for NSF-funded Head Start on Science, Co-PI for NSF-funded Advancing Research and its Impact on Society (ARIS) Center.
        </em>
      </p>

      <p>
        Dr. Van Egeren is the interim associate provost for University Outreach and Engagement and her work emphasizes community-engaged scholarships and translating research to the public.
      </p>
    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Frank Lawrence</a> <br/>
        </strong>
        <strong>Investigator</strong>, Senior Statistician <br />
        <em class="small d-inline-block w-75">
          Applied economic methods for economic modeling and for monitoring and evaluation.
        </em>
      </p>

      <p>
        Dr. Miller is the director of the Center for Economic Analysis, and serves as the lead economist focusing on the economic impacts of GSRP.
      </p>
    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Steven Miller</a> <br/>
        </strong>
        <strong>Investigator</strong>, Policy Economist <br />
        <em class="small d-inline-block w-75">
          Applied economic methods for economic modeling and for monitoring and evaluation.
        </em>
      </p>

      <p>
        Dr. Miller is the director of the Center for Economic Analysis, and serves as the lead economist focusing on the economic impacts of GSRP.
      </p>
    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Hope Akaeze</a> <br/>
        </strong>
        <strong>Investigator</strong>, Project Statistician <br />
      </p>

      <p>
        Dr. Akaeze is a Statistician for the evaluation of the Great Start Readiness Program.
      </p>

    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Miles McNall</a> <br/>
        </strong>
        <strong>Lead Advisor</strong>, Sociologist <br />
        <em class="small d-inline-block w-75">
          School-based health centers, childhood and youth mental health systems.
        </em>
      </p>

      <p>
        Dr. McNall is the Director of the Community Evaluation and Research Collaborative, and facilitates GSRP advisory committee meetings.
      </p>
    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Eb Weber</a> <br/>
        </strong>
        <strong>Data Manager</strong><br />
      </p>

      <p>
        Eb Weber is the Data Manager for the GSRP Evaluation project.
      </p>

    </div>

    <hr />

    <div class="mt-5">
      <p>
        <strong class="card-person">
          <a href="#">Teresa Herbowicz</a> <br/>
        </strong>
        <strong>Research Administrator's Assistant</strong><br />
      </p>

      <p>
        Teresa is a Research Administrative Assistant on the GSRP Evaluation project and other projects.
      </p>

    </div>



  </div>
</div>
