<?php
$page_title = "Research Reports";
?>

 <p>While public investment in preschool has been an increasingly popular social policy and there are numerous studies documenting its positive impacts, it is not without controversy. Most critics questioned whether it is worth the investment, as some studies suggested the impacts could be minimal or fade quickly. Others challenged the validity of the studies, as many existing findings were based on small samples or subject to selection bias.</p>

 <p>To build on the success of GSRP and help address these concerns, this evaluation study aims to answer four questions:</p>

<ul>
	<li>Is GSRP equitably accessible to 4-year-old eligible children across geographic, racial/ethnic and income subgroups?</li>
	<li>How do different GSRP quality and implementation strategies relate to preschool outcomes?</li>
	<li>What are the academic benefits of GSRP?</li>
	<li>What are the economic returns to ISDs and comparative cost-effectiveness?</li>
</ul>

<p><a href="gsrp-team">View the MSU team of researchers</a></p>

<br />

<h2>Papers</h2>

<ul>
	<li><a href="https://kappanonline.org/new-opportunities-family-engagement-covid-19-wilinski-morley-wu/">Uncovering new opportunities for family engagement during COVID-19</a></li>
	<li><a href="https://journals.sagepub.com/eprint/9N7QQHVD7VCTEB2CM6KZ/full">Resolving Dimensionality in a Child Assessment Tool: An Application of the Multilevel Bifactor Model</a></li>
</ul>

<br />

<h2>Briefs</h2>

<ul>
	<li><a href="https://cerc.msu.edu/upload/documents/KRA_3rd version_FINAL.pdf">Michigan Public Preschools Improve Kindergarten Readiness (August 2021)</a></li>
</ul>

<br />

<h2>Annual Reports</h2>

<ul>
	<li><a href="https://cerc.msu.edu/upload/gsrp/Great Start Readiness Program State Evaluation 2020-21 Annual Report.pdf">Great Start Readiness Program State Evaluation 2020-21 Annual Report</a></li>
	<li><a href="https://cerc.msu.edu/upload/gsrp/Great Start Readiness Program State Evaluation 2019-20 Annual Report.pdf">Great Start Readiness Program State Evaluation 2019-20 Annual Report</a></li>
	<li><a href="https://cerc.msu.edu/upload/documents/GSRP Annual Report 2018-19.pdf">Great Start Readiness Program State Evaluation 2018-19 Annual Report</a></li>
	<li><a href="https://cerc.msu.edu/upload/gsrp/GSRP Annual Report Year 1_Program Level.pdf">Great Start Readiness Program State Evaluation 2017-18 Annual Report. Part One: Accessibility and Program Quality</a></li>
	<li><a href="https://cerc.msu.edu/upload/gsrp/GSRP Annual Report_Year 1_Child_Level.pdf">Great Start Readiness Program State Evaluation 2017-18 Annual Report. Part Two: Child Eligibility Report</a></li>
</ul>
