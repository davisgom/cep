<?php
$page_title = "Research Briefs";
?>

<ul class="presentations" title="List of Research Briefs">
	<li><a href="https://cerc.msu.edu/upload/documents/21st CLCC Brief_CBO Run Programs.pdf">Schools and Communities Can Work Together to Provide Exceptional Out-of-School Time Experiences for Youth and Families</a> (September 2021)<br />
	During the 2020-21 program year, one in every five 21st CCLC programs was managed by schools but operated by community-based organizations. These programs were located in Grand Rapids, Wyoming, and Kalamazoo areas, serving 2,200 primarily Black (39%) and Hispanic (40%) low- income youth and families. The report presents feedback from youth and families regarding how these programs have impacted them under the pandemic.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/SEL-factsheet-2016.pdf">Social-Emotional Learning in Michigan 21st Century Community Learning Centers Afterschool Programs Linked to Improved School Behavior</a> (June 2016)<br />
	Using data from 2015-16, the data shows that students&#39; social-emotional learning experiences from Michigan 21st CCLC is associated with improved school behavior.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21stCCLC-Program-highlights_2.pdf">Activity Types and Frequent Attendance in Michigan 21st Century Community Learning Centers Linked to Improved Academic Performance</a> (June 2015)<br />
	In this two-pager, a summary of findings shows that Michigan 21st Century Community Learning Centers program participation was related to improved academic outcomes, and participation in different types of activities related to different outcomes.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/STEMLearning-factsheet-2014.pdf">STEM Learning Opportunities in Michigan 21st CCLC Programs</a> (April 2014)<br />
	In this fact sheet, a summary of findings shows that almost all Michigan 21st Century Community Learning Centers programs offer STEM activities, with most programs offering science and math every day. Participants included substantial numbers of women, Hispanics/Latinos, and African Americans, who are underrepresented in STEM professions.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21CCLC-01-AcademicOutcomes-factsheet-2013.pdf">Participation in Michigan 21st CCLC Programs Relates to Improvement in Academic Outcomes Fact Sheet</a> (August 2013)<br />
	In this fact sheet, a summary of findings shows that participation in Michigan 21st Century Community Learning Centers is associated with better academic outcomes, and this finding persists across different demographic and program factors.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21CCLC-02-HealthierYouth-factsheet-2013.pdf">Michigan 21st CCLC programs promote healthier youth! Fact Sheet</a> (August 2013)<br />
	In this fact sheet, a summary of findings shows participants reported benefits from health, nutrition and physical fitness activities.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21CCLC-FactSheet-2012-0221.pdf">2011 Before- and After-School Summer Expansion Grant Fact Sheet</a> (January 2012)<br />
	In this fact sheet, a summary of findings shows summer programs proved to be effective in helping students retain academic skills from the previous school year, improving students&#39; performance in reading and math, increasing students&#39; participation and engagement in summer programs, and promoting students&#39; development in other areas needed for success in school and life.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21CCLC-Brief10-2011-final.pdf">Benefits of the 2011 Before- and After-School Summer Expansion Grant Program</a> (December 2011)<br />
	In this brief, summer youth programs have the potential to promote academic skill building and positive youth development; however, in a period of tight budgets, the question arises of whether summer programs for children attending low-performing schools are a good value.</li>
	<li><a href="https://cerc.msu.edu/upload/documents/21CCLC-Brief09-2011-1117.pdf">Overview of Michigan&#39;s Program and the State Evaluation</a> (October 2011)<br />
	In this brief, an overview is provided of the purpose, goals, and funding priorities of the Michigan Program and State Evaluation.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21cclc_brief08_032511_hires.pdf">21st CCLC Programs in Michigan: What&#39;s Happening and How Students Benefit</a> (March 2011)<br />
	In this brief, we explain that the focus of the program is expanding access to academic enrichment opportunities, such as homework help, tutoring, and project-based academic learning, designed to help students meet local and state standards in core academic subjects.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief07_080610_hires.pdf">Coding Data</a> (August 2010)<br />
	In this brief, we explain what coding is, how activities and sessions are coded into categories and subcategories, and how to accurately describe your program&#39;s activities and sessions.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//YouthPartic_hires.pdf">Why Youth Choose to Participate in 21st CCLC After-school Programs</a> (July 2010)<br />
	In this brief, we discuss how participating in after-school programs can help youth to improve academically and promote positive development, especially among low-achieving students attending low-performing schools.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief05_052510_hires.pdf">Using Data for Program Improvement, Part II</a> (May 2010)<br />
	In this brief, we discuss how to interpret data to understand what it says about a 21st CCLC program&rsquo;s strengths and weaknesses. We also discuss possible explanations for results and how to use the findings to develop a program improvement plan.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief_04_hires.pdf">Using Data for Program Improvement, Part I</a> (August 2009)<br />
	In this brief, we define basic evaluation terms and show you how to read data presented in different types of charts and tables.</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief_03_hires.pdf">Finding the Link between 21st CCLC and Regular Classrooms: Communicating with School-Day Staff</a> (December 2007)<br />
	This brief reports on communications between school-day and out-of-school-time staff, using data from six Michigan 21st CCLC programs serving students at 10 sites.<br />
	<a href="https://cerc.msu.edu/upload/documents//21CCLC_FindingTheLink_tech_report.pdf">Technical Supplement to Finding the Link between 21st CCLC and Regular Classrooms</a></li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief_02_hires.pdf">Participation in Michigan&#39;s 21st CCLC: Findings from the State Evaluation</a> (August 2007)<br />
	This brief reports on student participation in Michigan programs, using data from the state evaluation conducted by the Community Evaluation and Research Collaborative (CERC).</li>
	<li><a href="https://cerc.msu.edu/upload/documents//21CCLC_brief_01_hires.pdf">Overview Brief</a> (March 2007)<br />
	This Research Brief is the first in a series of evaluation reports about the 21st CCLC programs in Michigan. We present an overview of the program, the evaluation process in Michigan, and the current research on factors that lead to better outcomes for youth in after-school programs. Subsequent briefs in the series will examine these factors in more detail.<br />
	<a href="https://cerc.msu.edu/upload/documents//21CCLC_Overview_tech_report.pdf">Technical Supplement to the Overview</a></li>
</ul>
