<?php
$page_title = "Overview";
?>

<div  class="feature-image">
  <img src="Content/Images/21st-century.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  The 21st Century Community Learning Centers (21st CCLC) are federally funded out-of-school time programs for high-poverty, low-performing schools administered by state departments of education. This project is an ongoing comprehensive evaluation of statewide program implementation and effectiveness for 21st CCLC programs in about 300 sites coordinated by 50 grantees across Michigan. The evaluation is centered around identifying characteristics important for continuous program improvement and better academic and socioemotional outcomes. To communicate results, the evaluation team develops briefs to show how students can benefit from program participation with improved academic outcomes, STEM involvement, health, nutrition and fitness education, and Social-Emotional Learning (SEL).
</p>

<p>
  The team has also developed a state-of-the-art data collection system in conjunction with <br /> <a href="http://www.cypq.org/" <?php echo $external; ?>>David P. Weikart Center for Youth Program Quality</a> and <a href="http://www.thomaskelly.com/" <?php echo $external; ?> >Thomas Kelly Software Associates</a>.
</p>

<br  />

<div class="card">
  <div class="card-header">
    <h2>People:</h2>
  </div>

  <div class="card-body">
    <p class="w-75">
      <strong class="card-person">
        <a href="#">Jamie Heng-Chieh Wu</a> <br/>
      </strong>
      <strong>Principal Investigator</strong>, 21st CCLC State Evaluation, GSRP State Evaluation, Expanding Youth-Driven Space Across Southeast Michigan <br />
      <strong>Associate Director for Community Evaluation Programs,</strong> Office for Public Engagement and Scholarship
    </p>

    <hr />

    <p class="w-75">
      <strong class="card-person">
        <a href="#">Laurie A. Van Egeren</a> <br/>
      </strong>
      <strong>Principal Investigator</strong>, 21st CCLC State Evaluation<br />
      <strong>Interim Associate Provost for University Outreach and Engagement</strong>
      <strong>Adjunct Faculty</strong>, Department of Human Development and Family Studies
    </p>
  </div>
</div>

<br />

<div class="card">
  <div class="card-body">
      <dl>
        <dt>
          <strong>Additional CERC members:</strong>
        </dt>
        <dd>
          Beth Prince; Nai-Kuan Yang; Debbie Stoddard
        </dd>

        <dt>
          <strong>Funder:</strong>
        </dt>
        <dd>
          Michigan Department of Education
        </dd>

        <dt>
          <strong>MSU Partners:</strong>
        </dt>
        <dd>
          Community Evaluation and Research Collaborative; College of Education; Department of Family and Child Ecology
        </dd>

        <dt>
          <strong>Community Partners:</strong>
        </dt>
        <dd>
          Michigan Department of Education; David P. Weikart Center for Youth Program Quality; Michigan After-School Partnership; Michigan Afterschool Association; Thomas Kelly Software Associates; Learning Point Associates; 50 school districts and community-based organizations across Michigan
        </dd>
      </dl>
    </div>
  </div>
