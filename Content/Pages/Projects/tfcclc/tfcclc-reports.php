<?php
$page_title = "Reports";
?>

<h3 class="mb-4">Leading Indicators Reports</h3>

<ul class="presentations" title="List of Leading Indicators Reports">
	<li>Wu, J. &amp; Van Egeren, L., A. (2019). <a href="https://cerc.msu.edu/upload/documents/Leading Indicators Report for 2018-19.pdf">Michigan 21st Century Community Learning Centers leading Indicators report for 2018-19</a>. Michigan State University, East Lansing, MI.</li>
	<li>Wu, J. &amp; Van Egeren, L., A. (2019). <a href="https://cerc.msu.edu/upload/documents/Leading Indicators Interpretation Guide November 2019_FINAL.docx">Michigan 21st Century Community Learning Centers leading Indicators interpretation guide for 2018-19</a>. Michigan State University, East Lansing, MI.</li>
	<li>Michigan 21st CCLC State Evaluation Team. (2019).&nbsp;<a data-auth="NotApplicable" href="https://cerc.msu.edu/upload/documents/Grantee Data Table.pdf" rel="noopener noreferrer" target="_blank">Michigan 21st Century Community Learning Centers Grantee Annual Data Tables</a>. Michigan State University, East Lansing, MI.</li>
	<li>Michigan 21st CCLC State Evaluation Team. (2019).&nbsp;<a data-auth="NotApplicable" href="https://cerc.msu.edu/upload/documents/Site Data Table.pdf" rel="noopener noreferrer" target="_blank">Michigan 21st Century Community Learning Centers Site Annual Data Tables</a>. Michigan State University, East Lansing, MI.</li>
	<li>Wu, H-C., Van Egeren, L.A. &amp; Weikart Center TACSS Team (2016). <a href="https://cerc.msu.edu/upload/documents/FAQ-Leading-Indicator-Reports-2pg.pdf">Frequently Asked Questions About the Leading Indicators Report</a>. Michigan State University, East Lansing, MI.</li>
	<li>Van Egeren, L. A., Wu, H-C., Garner, A., &amp; Smith, C. (2012, October). <a href="https://cerc.msu.edu/upload/documents/21stCCLCDemonstrationSessionAEA 2012_FINAL.pdf">Using cost-effective processes to develop large-scale data-driven continuous improvement systems for local programs</a>. Demonstration session presented at the American Evaluation Association, Minneapolis, MN.</li>
</ul>

<h3 class="mb-4 mt-5">Technical Reports</h3>

<ul class="presentations" title="List of Technical Reports">
	<li>Wu, H. C. &amp; Van Egeren, L. A. (2021, May). <a href="https://cerc.msu.edu/upload/documents/21stCCLC Annual Evaluation Report_2019-2020_Final.pdf">Michigan 21st Century Community Learning Centers Evaluation 2019-20 Annual Report</a>.</li>
	<li>Wu, H. C. &amp; Van Egeren, L. A. (2020, April). <a href="https://cerc.msu.edu/upload/documents/21stCCLC Annual Evaluation Report_2018-2019_FINAL.pdf">Michigan 21st Century Community Learning Centers Evaluation 2018-2019&nbsp;Annual Report</a>.</li>
	<li>Wu, H. C. &amp; Van Egeren, L. A. (2019, April). <a href="https://cerc.msu.edu/upload/documents/21stCCLC Annual Evaluation Report_2017-2018_Final_11122019_acc.pdf">Michigan 21st Century Community Learning Centers Evaluation 2017-2018 Annual Report</a>.</li>
	<li>Wu, H. C. &amp; Van Egeren, L. A. (2018, April). <a href="https://cerc.msu.edu/upload/documents/21stCCLC Annual Evaluation Report_2016-2017-3.pdf">Michigan 21st Century Community Learning Centers Evaluation 2016-2017 Annual Report</a>.</li>
	<li>Wu, H. C. &amp; Van Egeren, L. A.&nbsp;(2017, March). <a href="https://cerc.msu.edu/upload/documents/21stCCLC-Annual-Evaluation-Report_2015-2016_FINAL.pdf">Michigan 21st Century Community Learning Centers Evaluation 2015-2016 Annual Report</a>.</li>
	<li>Wu, H. C., Van Egeren, L. A., &amp; Bates, L. V. (2016, June). <a href="https://cerc.msu.edu/upload/documents/21stCCLC-Annual-Evaluation-Report_2014-2015_FINAL.pdf">Michigan 21st Century Community Learning Centers Evaluation 2014-2015 Annual Report</a>.</li>
	<li>Wu, H. C., Van Egeren, L. A., &amp; Bates, L. V. (2015, June). <a href="https://cerc.msu.edu/upload/documents/21stCCLC-Annual-Evaluation-Report_2013-2014_Submitted.pdf">Michigan 21st Century Community Learning Centers Evaluation 2013-2014 Annual Report</a>.</li>
	<li>Wu, H. C., Van Egeren, L. A., &amp; Bates, L. V. (2014, June). <a href="https://cerc.msu.edu/upload/documents/21stCCLC-AR-2014_FINAL.pdf">Michigan 21st Century Community Learning Centers Evaluation 2012-2013 Annual Report</a>.</li>
	<li>Reed, C. S., Van Egeren, L. A., &amp; Bates, L. V. (2012, July). <a href="https://cerc.msu.edu/upload/documents/21stCCLCAnnualReport 2010-11Final.pdf">Michigan 21st Century Community Learning Centers Evaluation 2010-2011 Annual Report</a>.</li>
	<li>Van Egeren, L. A., Wu, H. C., &amp; Kornbluh, M. (2012, April). <a href="https://cerc.msu.edu/upload/documents/NZFinalEvaluationReport-P3013723.pdf">Evaluation of the Youth-Driven Spaces Project Final Report</a>.</li>
	<li>Van Egeren, L. A., Bates, L. V., Lee, K. S., &amp; Reed, C. S. (2011, October). <a href="https://cerc.msu.edu/upload/documents/DHSSummerExpansionReport_FINAL_v3.pdf">21st Century Community Learning Centers Summer Expansion Grant Final Evaluation Report</a>.</li>
	<li>Reed, C.S., Van Egeren, L. A., &amp; Bates, L. V. (2011, August). <a href="https://cerc.msu.edu/upload/documents/21stCCLCAnnualReport2009-10_FINAL.pdf">Michigan 21st Century Community Learning Centers Evaluation annual report, 2009-2010</a>.</li>
	<li>Reed, C.S., Van Egeren, L. A., &amp; Bates, L. V. (2009, December). <a href="https://cerc.msu.edu/upload/documents/08-09_Final_Annual_Report.pdf">Michigan 21st Century Community Learning Centers state evaluation annual report, 2008-2009</a>. Report for Michigan Department of Education.</li>
	<li>Reed, C.S., Van Egeren, L. A., &amp; Bates, L. V. (2009, August). <a href="https://cerc.msu.edu/upload/documents/09-09-08_Final.pdf">Michigan 21st Century Community Learning Centers state evaluation annual report, 2007-2008</a>. Report for Michigan Department of Education.</li>
	<li>Reed, C.S., Van Egeren, L. A., &amp; Bates, L. V. (2009, June). <a href="https://cerc.msu.edu/upload/documents/08_ARF_Report_to_MDE_for_06-07.pdf">21st Century Community Learning Centers state evaluation annual report, 2006-2007</a>. Report for Michigan Department of Education.</li>
	<li>Reed, C.S., Van Egeren, L. A., &amp; Bates, L. V. (2008, April). <a href="https://cerc.msu.edu/upload/documents/ARF_report_2005-06-FINAL.pdf">21st Century Community Learning Centers state evaluation annual report, 2005-2006</a>. Report for Michigan Department of Education.</li>
	<li>Bates, L. V., Reed, C. S., Donahue, T. S., Prince, B., Smith, A., &amp; Van Egeren. L. A. (2004, March). <a href="https://cerc.msu.edu/upload/documents/21stCCLC_2004_midyear_Final.pdf">21st Century Community Learning Centers state evaluation: Mid-year implementation report, July, 2003 - February, 2004</a>. East Lansing, MI: University Outreach and Engagement.</li>
	<li>Reed, C. S., Bates, L. V., Van Egeren, L. A., Baker, D., Dunbar, C., Smith, B., Villarruel, F., Donohue, T., Smith, A., &amp; Hahn, H. A. (2003, September). <a href="https://cerc.msu.edu/upload/documents/21stCentury_2003.pdf">21st Century Community Learning Centers state evaluation implementation report--January-June 2003</a>. Report for Michigan Department of Education.</li>
	<li>Van Egeren, L. A., Bates, L., &amp; Reed, C. S. (2003, June). <a href="https://cerc.msu.edu/upload/documents/21stCentury_2003_baseline.pdf">21st Century Community Learning Centers state evaluation report: Implementation of the state evaluation/baseline data</a>. Report for Michigan Department of Education.</li>
</ul>

<h3 class="mb-4 mt-5">Other Reports</h3>

<ul class="presentations">
	<li>Wilson-Ahlstrom, A., Yohalem, N., &amp; Pittman, K. (2007, March). <a href="http://www.forumfyi.org/Files//Building_Quality_Improvement_Systems.pdf">Building Quality Improvement Systems: Lessons from Three Emerging Efforts in the Youth-Serving Sector, March, 2007</a>. Washington D.C.: Forum and Impact Strategies, Inc.</li>
</ul>
