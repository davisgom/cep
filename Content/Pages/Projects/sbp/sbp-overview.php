<?php
$page_title = "Overview";
?>

<style>
@media (min-width: 992px){ .feature-image img {margin-top: 200px; }
</style>

<div  class="feature-image">
  <img src="/Upload/banners/strong-beginnings_original.jpg" class="d-block w-100 img-fluid" alt="">
</div>

<p>
  Our evaluation project examines the implementation, quality, and impacts of 12 publicly funded preschool classrooms for 3-year-old children. Classrooms are currently located in four Michigan counties and managed by Berrien, Heritage Southwest, Northwest Education, and Wayne Intermediate School Districts. Our research agenda includes: (a) examining the impacts of attending two years vs. one-year or no experience in public preschools; (b) collecting input from teachers and families on effective family engagement practices to create a family engagement implementation guide for use as the program expands. The guide will reflect the needs and goals of teachers and caregivers—because our research design involved them from the beginning.
</p>