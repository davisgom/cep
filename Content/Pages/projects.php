<h1>
  <?php echo str_replace("-", " ", ucfirst($page_content));?>


</h1>

<?php //include ("Views/Shared/Partials/Carousel.php"); ?>

<!-- <hr class="my-5" /> -->

<section class="projects-list">
    <div class="container">
      <div class="row">
          <div class="col-md-12">
              <?php include 'Views/Shared/Partials/projects-list.php'; ?>

              <hr />

              <ul class="row">
                <li class="col-md-6">
                  <h3>Youth-Adult<br /> Partnership RUBRIC</h3>

                  <p class="project-caption">
                    <?php echo $rubric; ?>
                  </p>

                    <a class="btn btn-theme btn-theme-secondary-outline" href="rubric-overview">Learn More</a>
                </li>

                <li class="col-md-6">
                    <br class="d-none d-md-block" />
                    <h3>Strong Beginnings Program</h3>

                    <p class="project-caption">
                      <?php echo $sbp; ?>
                    </p>

                    <a class="btn btn-theme btn-theme-secondary-outline" href="sbp-overview">Learn More</a>
                </li>
              </ul>

          </div>
      </div>

        </div>



    </div>
</section>
