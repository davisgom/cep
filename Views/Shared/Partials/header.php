<div id="skip-nav" class="sr-only">
    <ul>
        <li><a href="#siteNav">Skip to Main Navigation</a></li>
        <li><a href="#Main">Skip to the Content</a></li>
        <li><a href="#Footer">Skip to the Footer</a></li>
    </ul>
</div>

<header class="site-header">
  <?php include("msu-masthead.php"); ?>

  <section class="site-masthead">
    <div class="container">
      <div class="row">
        <div class="site-branding col-12 col-lg">
          <h1><a href="home"><?php echo $site_title; ?></a></h1>
          <p>Office for Public Engagement and Scholarship</p>
          <p>University Outreach and Engagement</p>
        </div>

        <div class="col-12 col-lg">
          <?php include("nav.php"); ?>
        </div>
      </div>
    </div>

    <!-- <div class="site-navigation">
      <div class="container">
        <div class="row">
          <div class="col">
            <?php //include("nav.php"); ?>
          </div>
        </div>
      </div>
    </div> -->
  </section>
</header>
