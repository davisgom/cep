<?php
$site_title = "Community Evaluation Programs";

$keywords = "outreach engagement scholar scholarship community development responsive build capacity building collaborative collaboration michigan state university";

$author = "University Outreach and Engagement - Communication and Information Technology";

$description = "University Outreach and Engagement is a campus-wide central resource that assists MSU academic units construct more effective engagement with communities.";

$main_phone = "(517) 353-8977";

$external = 'target="_blank" rel="noopener" data-toggle="tooltip" data-placement="bottom" title="Link opens in new window"';

$email = 'target="_blank" rel="noopener" data-toggle="tooltip" data-placement="bottom" title="E-Mail"';

$tfcc = "The 21<sup>st</sup> Century Community Learning Centers (21st CCLC) are federally funded out-of-school time programs for high-poverty, low-performing schools administered by state departments of education.";

$gsrp = "Mchigan's Great Start Readiness Program (GSRP) is a state-funded preschool program for four-year-old children with factors that place them at risk for educational failure. The program provides high-quality academic and social-emotional experiences that prepare Michigan children for kindergarten.";

$rubric = "The Rubric is developed through a research study consisting of extensive literature view, a series of program observations, youth and adult interviews and focus groups to assess the practices of Youth-Adult Partnership (Y-AP) in various youth settings.";

$yds = "The Youth-Driven Space (YDS) refers to a positive youth development environment where youth partner with adults to meaningfully engage in decision-making throughout the organization’s governance and programming activities.";

$mapping = "The Childcare Mapping Project was awarded by the Michigan Department of Education (MDE) as part of the Caring for MI Future initiative. Caring for MI Future is a $100 million investment that will help more Michigan families find quality, affordable child care in their community by opening 1,000 new or expanded child care programs by the end of 2024.";

$sbp = "Our evaluation project examines the implementation, quality, and impacts of 12 publicly funded preschool classrooms for 3-year-old children. Classrooms are currently located in four Michigan counties and managed by Berrien, Heritage Southwest, Northwest Education, and Wayne Intermediate School Districts.";

// 21st Century Community Learning Centers
$tfcclc_Dir = "Content/Pages/Projects/tfcclc";
$tfcclc_File = array_diff(scandir($tfcclc_Dir), array('.', '..'));
$tfcclc_Files = array();

foreach($tfcclc_File as $file){
  $name = strstr($file, '.', TRUE);
  $tfcclc_Files[] = $name;
}

// Great Start Readiness Program
$gsrp_Dir = "Content/Pages/Projects/gsrp";
$gsrp_File = array_diff(scandir($gsrp_Dir), array('.', '..'));
$gsrp_Files = array();

foreach($gsrp_File as $file){
  $name = strstr($file, '.', TRUE);
  $gsrp_Files[] = $name;
}

// Childcare Mapping Project
$cmp_Dir = "Content/Pages/Projects/cmp";
$cmp_File = array_diff(scandir($cmp_Dir), array('.', '..'));
$cmp_Files = array();

foreach($cmp_File as $file){
  $name = strstr($file, '.', TRUE);
  $cmp_Files[] = $name;
}

// Youth-Adult Partnership RUBRIC
$rubric_Dir = "Content/Pages/Projects/rubric";
$rubric_File = array_diff(scandir($rubric_Dir), array('.', '..'));
$rubric_Files = array();

foreach($rubric_File as $file){
  $name = strstr($file, '.', TRUE);
  $rubric_Files[] = $name;
}

// Youth Driven Space
$yds_Dir = "Content/Pages/Projects/yds";
$yds_File = array_diff(scandir($yds_Dir), array('.', '..'));
$yds_Files = array();

foreach($yds_File as $file){
  $name = strstr($file, '.', TRUE);
  $yds_Files[] = $name;
}


// Youth Driven Space
$sbp_Dir = "Content/Pages/Projects/sbp";
$sbp_File = array_diff(scandir($sbp_Dir), array('.', '..'));
$sbp_Files = array();

foreach($sbp_File as $file){
  $name = strstr($file, '.', TRUE);
  $sbp_Files[] = $name;
}

?>
