<button class="menu-btn btn btn-light d-md-block d-lg-none d-print-none" onclick="navToggle()" onkeydown="navToggle()">
  <span class="menu-icon">
    <i class="fas fa-bars"></i> <br /> <span>Menu</span>
  </span>
  <span class="search-icon">
    <i class="fas fa-search fa-lg"></i>
  </span>
</button>

<section id="siteNav" class="overlay">
  <div class="overlay-content">
    <div class="container">
      <a class="closebtn d-lg-none" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>

      <div id="MSUSearchTool" class="no-fill d-lg-none" role="search">
    			<?php include("msu-search.php"); ?>
    	</div>

      <nav class="row">
        <h1 class="sr-only">Site Navigation</h1>
        <ul class="nav nav-pills nav-fill site-nav" role="navigation">
          <li>
            <a class="nav-link" href="home">
              Home
            </a>
          </li>

          <li>
            <a class="nav-link <?php if ($page_content == "about") { echo "active";}?>" href="about">
              About
            </a>
          </li>



          <!-- <li>
            <a class="nav-link <?php if ($page_content == "projects") { echo "active";}?>" href="projects">
              Projects
            </a>
          </li> -->

          <li>
            <a class="nav-link dropdown-toggle <?php if ($page_content == "projects") { echo "active";}?>" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Projects
            </a>

            <div class="dropdown-menu">
              <span>
                <a class="dropdown-item" href="tfcclc-overview">21st Century Community Learning Centers</a>
                <a class="dropdown-item" href="gsrp-overview">Great Start Readiness Program</a>
                <a class="dropdown-item" href="cmp-overview">Childcare Mapping Project</a>
                <a class="dropdown-item" href="yds-overview">Youth-Driven Space</a>
                <a class="dropdown-item" href="rubric-overview">Youth-Adult Partnership RUBRIC</a>
                <a class="dropdown-item" href="sbp-overview">Strong Beginnings Program</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="projects">View all Projects</a>
              </span>
            </div>
          </li>

          <li>
            <a class="nav-link <?php if ($page_content == "people") { echo "active";}?>" href="#">
              People
            </a>
          </li>

          <li>
            <a class="nav-link <?php if ($page_content == "contact") { echo "active";}?>" href="contact">
              Contact
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </div>

  <div class="container closebtn-bottom d-lg-none">
		<a class="closebtn" href="javascript:void(0)" onclick="navToggle()"><i class="fa fa-times" aria-hidden="true"></i> CLOSE</a>
	</div>
</section>
