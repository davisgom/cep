<header class="subpage-header">
  <section class="subpage-title tfcclc-subpage">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-11 py-5 d-flex">
          <h1>
            <span class="subpage-section-title">21<sup>st</sup> Century Community Learning Centers</span><br />
            <?php echo $page_title ?>
          </h1>
      </div>
    </div>
  </section>
</header>
