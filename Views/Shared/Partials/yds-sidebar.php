<?php $has_sidebar = "true"; ?>

<div class="col-md-3 py-5 sidebar">
  <section>
    <div class="container">
      <nav class="row">
        <h1 class="sr-only">The Evaluation of Expanding Youth-Driven Space Across Southeast Michigan Centers Navigation</h1>
        <ul class="nav flex-column nav-pills nav-fill subpage-nav text-capitalize" role="navigation">
          <li>
            <a class="nav-link" href="yds-overview">
              Overview
            </a>
          </li>

          <li>
        		<a class="nav-link" href="yds-about">
              About
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Research Team
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Contact
            </a>
        	</li>
        </ul>
      </nav>
    </div>
  </section>
</div>