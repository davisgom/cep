<ul class="row">
    <li class="col-md-6">
      <a href="tfcclc-overview">
        <img src="/Upload/banners/21st-century.jpg" alt="" class="img-fluid" />
      </a>

        <h3>Michigan 21<sup>st</sup> Century Community<br /> Learning Centers</h3>

        <p class="project-caption">
          <?php echo $tfcc; ?>
        </p>

        <a class="btn btn-theme btn-theme-secondary-outline" href="tfcclc-overview">Learn More</a>
    </li>

    <li class="col-md-6">
        <a href="gsrp-overview" style="display: block; max-height: 330px; overflow: hidden;">
          <img src="/Upload/banners/gsrp_original.jpg"  alt="" class="img-fluid" />
        </a>
        
        <h3>Great Start<br /> Readiness Program</h3>

        <p class="project-caption">
          <?php echo $gsrp; ?>
        </p>

        <a class="btn btn-theme btn-theme-secondary-outline" href="gsrp-overview">Learn More</a>
    </li>

     <li class="col-md-6">
        <a href="cmp-overview">
          <img src="/Upload/banners/mapping.jpg" alt="" class="img-fluid">
        </a>
        <h3>Child Care<br /> Mapping Project</h3>

        <p class="project-caption">
          <?php echo $mapping; ?>
        </p>

        <a class="btn btn-theme btn-theme-secondary-outline" href="cmp-overview">Learn More</a>
    </li>


    <li class="col-md-6">
        <a href="sbp-overview">
          <img src="/Upload/banners/strong-beginnings_original.jpg" alt="" class="img-fluid">
        </a>

        <h3>Strong Beginnings<br /> Program</h3>

        <p class="project-caption">
          <?php echo $sbp; ?>
        </p>

        <a class="btn btn-theme btn-theme-secondary-outline" href="sbp-overview">Learn More</a>
    </li>
</ul>
