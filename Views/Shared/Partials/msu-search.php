<div id="MSUSearchTool" class="no-fill" role="search">
  <div id="search-tool-box" >
    <!-- u.search.msu.edu BEGIN -->

    <!-- action should be equal to “//u.search.msu.edu/index.php” when using Search Tool -->
    <form action="" method="get">

      <!-- value should be equal to site name. Ex: University Archives -->
      <input type="hidden" name="client" value="" />

      <!-- value should be equal to the hostname that will be searched.
      Ex: “archives.msu.edu”-->
      <input type="hidden" name="sitesearch" value="" />

      <!-- OPTIONAL - value should be equal to your Google analytics number ########-# -->
      <input type="hidden" name="analytics" value="" />

      <!-- u.search.msu.edu END -->

      <!-- The styles applied to the following input fields and buttons comply with brand
      standards and should be used regardless of your search solution -->

      <label class="hide" for="q">Search Tool </label>
      <input id="q" name="q" placeholder="Search..."/>
      <input id="btn_search" type="submit" value="search button" />
    </form>
  </div>
</div>
