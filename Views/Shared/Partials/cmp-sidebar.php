<?php $has_sidebar = "true"; ?>

<div class="sidebar col-md-3 py-5" style="background-color: aqua;">
  <section>
    <div class="container">
      <nav class="row">
        <h1 class="sr-only">Childcare Mapping Project Navigation</h1>
        <ul class="nav flex-column nav-pills nav-fill subpage-nav text-capitalize" role="navigation">
          <li>
            <a class="nav-link" href="cmp-overview">
              Overview
            </a>
          </li>

          <li>
        		<a class="nav-link disabled" href="#">
              About
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Research Team
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Contact
            </a>
        	</li>
        </ul>
      </nav>
    </div>
  </section>
</div>