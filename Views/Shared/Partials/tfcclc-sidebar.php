<?php $has_sidebar = "true"; ?>

<div class="col-md-3 py-5 sidebar" style="background-color: aqua;">
  <section>
    <div class="container">
      <nav class="row">
        <h1 class="sr-only">21<sup>st</sup> Century Community Learning Centers Navigation</h1>
        <ul class="nav flex-column nav-pills nav-fill subpage-nav text-capitalize" role="navigation">
          <li>
            <a class="nav-link" href="tfcclc-overview">
              Overview
            </a>
          </li>

          <li>
        		<a class="nav-link" href="tfcclc-briefs">
              Research Briefs
            </a>
        	</li>

          <li>
        		<a class="nav-link" href="tfcclc-reports">
              Reports
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Presentations
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Surveys
            </a>
        	</li>

          <li>
        		<a class="nav-link disabled" href="#">
              Resources
            </a>
        	</li>
        </ul>
      </nav>
    </div>
  </section>
</div>