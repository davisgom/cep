<?php $has_sidebar = "true"; ?>

<div class="sidebar col-md-3 py-5">      
  <section>
    <div class="container">
      <nav class="row">
        <h1 class="sr-only">Great Start Readiness Program Navigation</h1>
        <ul class="nav flex-column nav-pills nav-fill subpage-nav text-capitalize" role="navigation">
          <li>
            <a class="nav-link" href="gsrp-overview">
              Overview
            </a>
          </li>

          <li>
            <strong class="subnav-header">
              Maps
            </strong>
            
            <ul class="sidebar-subnav">
              <span>
                <li><a class="nav-link" href="gsrp-site-locations">ISD and Site Locations</a></li>
                <li><a class="nav-link" href="#">Classroom Characteristics</a></li>
                <li><a class="nav-link" href="#">ISD Comparison</a></li>
                <li><a class="nav-link" href="#">ISD Comparison Over Time</a></li>
                <li><a class="nav-link" href="#">Sites by Child Opportunity Index</a></li>
                <li><a class="nav-link" href="#">Service Utilization by Race/Ethnicity</a></li>
                <li><a class="nav-link" href="#">Staff vs Children Racial Disparities</a></li>
              </span>
            </ul>

            
          </li>

          <li>
        		<a class="nav-link" href="gsrp-reports">
              Research Reports
            </a>
        	</li>

          <li>
        		<a class="nav-link" href="gsrp-team">
              MSU Team
            </a>
        	</li>

        </ul>
      </nav>
    </div>
  </section>
</div>