<header class="subpage-header">
  <section class="subpage-title cmp-subpage">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-11 py-5 d-flex">
          <h1>
            <span class="subpage-section-title">Childcare Mapping Project</span><br />
            <?php echo $page_title ?>
          </h1>
      </div>
    </div>
  </section>
</header>
