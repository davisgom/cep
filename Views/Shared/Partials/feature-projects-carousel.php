<section class="container-fluid">
  <div class="row align-items-center">

    <div id="cep-feature-projects" class="col-12 p-0 carousel carousel-fade slide" data-ride="carousel" data-interval="5000">

      <div class="carousel-controls">
        <button id="playButton" onclick="carousel_playButton()" role="button" class="mr-2 active-button">
          <span class="fas fa-play" aria-hidden="true"></span>
          <span class="sr-only">Play</span>
        </button>

        <button id="pauseButton" onclick="carousel_pauseButton()" role="button">
          <span class="fas fa-pause" aria-hidden="true"></span>
          <span class="sr-only">Pause</span>
        </button>
      </div>

      <ol class="carousel-indicators">
        <li data-target="#cep-feature-projects" data-slide-to="0" class="active"></li>
        <li data-target="#cep-feature-projects" data-slide-to="1"></li>
        <li data-target="#cep-feature-projects" data-slide-to="2"></li>
        <li data-target="#cep-feature-projects" data-slide-to="3"></li>
      </ol>

      <div class="carousel-inner">
      <!-- CAROUSEL SLIDES START HERE -->

        <div class="carousel-item active">

          <div class="carousel-caption row">
            <div class="col-lg-5">
              <h2>
                21<sup>st</sup> Century Community Learning Centers
              </h2>
              <p class="">
                <?php echo $tfcc; ?>
              </p>

              <a href="tfcclc-overview" class="btn btn-theme btn-theme-small btn-theme-accent">Learn More</a>
            </div>

            <div class="col-lg-7 project-photo">
              <img src="Content/Images/21st-century.jpg" class="d-block img-fluid" alt="">
            </div>
          </div>
        </div>

        <div class="carousel-item">
          <div class="carousel-caption row">
            <div class="col-lg-5">
              <h2>
                Great Start Readiness Program
              </h2>
              <p class="">
                <?php echo $gsrp; ?>
              </p>

              <a href="gsrp-overview" class="btn btn-theme btn-theme-small btn-theme-accent">Learn More</a>
            </div>

            <div class="col-lg-7 project-photo">
              <img src="Content/Images/gsrp.jpg" class="d-block img-fluid" alt="">
            </div>
          </div>
        </div>

        <div class="carousel-item">
          <div class="carousel-caption row">
            <div class="col-lg-5">
              <h2>
                Childcare Mapping Project
              </h2>
              <p class="">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>

              <a href="#" class="btn btn-theme btn-theme-small btn-theme-accent">Learn More</a>
            </div>

            <div class="col-lg-7 project-photo">
              <img src="Content/Images/mapping.jpg" class="d-block img-fluid" alt="">
            </div>
          </div>
        </div>

        <div class="carousel-item">
          <div class="carousel-caption row">
            <div class="col-lg-5">
              <h2>
                Youth-Driven Space
              </h2>
              <p class="">
                <?php echo $yds; ?>
              </p>

              <a href="yds-overview" class="btn btn-theme btn-theme-small btn-theme-accent">Learn More</a>
            </div>

            <div class="col-lg-7 project-photo">
              <img src="Content/Images/yds.jpg" class="d-block img-fluid" alt="">
            </div>
          </div>
        </div>



      <!-- CAROUSEL SLIDES ENDS HERE -->
      </div>

      <a class="carousel-control-prev" href="#cep-feature-projects" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#cep-feature-projects" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</section>
