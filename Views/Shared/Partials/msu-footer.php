<!-- if your website has a dark background, change the 'light-background' class in the footer tag to 'dark-background', the links in the footer will change color to reflect this -->
	<section id="MSUstandardFooter" class="light-background row align-items-center"  role="contentinfo">

		<section class="col-xs-12 col-lg-3 pr-lg-0" id="standard-footer-MSU-wordmark">
				<a href="http://www.msu.edu" class="footer-MSU-wordmark">

					<!-- If using a dark background change the img src to images/msu-wordmark-white-221x47.png-->
					<img class="screen-msuwordmark img-fluid" alt="Michigan State University Wordmark" src="Content/Images/msu-wordmark-green.svg"/>
					<img class="print-msuwordmark img-fluid" alt="Michigan State University Wordmark" src="Content/Images/msu-wordmark-black.svg"/>

				</a>
		</section>

		<section class="col-xs-12 col-lg-9">
			<div id="standard-footer-site-links">
				<ul>
					<li>Call us: <strong><?php echo $main_phone; ?></strong></li>
					<li><a href="#">Contact Information</a></li>
					<li><a href="#">Site Map</a></li>
					<li><a href="#">Privacy Statement</a></li>
					<li><a href="#">Site Accessibility</a></li>
				</ul>

			</div>

			<div id="standard-footer-MSU-info" class="w-100">

					<ul>
						<li>Call MSU: <span class="msu-phone"><strong>(517) 355-1855</strong></span></li>
						<li>Visit: <strong><a href="http://msu.edu">msu.edu</a></strong></li>
						<li>MSU is an affirmative-action, <br class="d-block d-sm-none" /> equal-opportunity employer. </li>
						<li><a href="http://oie.msu.edu/"><strong>Notice of Nondiscrimination</strong></a></li>
					</ul>

					<ul>
						<li class="spartans-will">Spartans Will.</li>
						<li class="copyright">&#169; Michigan State University</li>
					</ul>
			</div>
		</section>


  </section>
