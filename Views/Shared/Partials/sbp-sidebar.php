<?php $has_sidebar = "true"; ?>

<div class="col-md-3 py-5 sidebar">
  <section>
    <div class="container">
      <nav class="row">
        <h1 class="sr-only">Strong Beginnings Program Navigation</h1>
        <ul class="nav nav-pills nav-fill subpage-nav text-capitalize" role="navigation">
          <li>
            <a class="nav-link" href="sbp-overview">
              Overview
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </section>
</div>