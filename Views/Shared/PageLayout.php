<?php

// 21st Century Community Learning Centers
if (in_array($page_content, $tfcclc_Files)) {
  ob_start(); include ("Content/Pages/Projects/tfcclc/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/tfcclc-header.php");
}

// Great Start Readiness Program
elseif (in_array($page_content, $gsrp_Files)) {
  ob_start(); include ("Content/Pages/Projects/gsrp/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/gsrp-header.php");
}

// Childcare Mapping Project
elseif (in_array($page_content, $cmp_Files)) {
  ob_start(); include ("Content/Pages/Projects/cmp/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/cmp-header.php");
}

// Youth-Adult Partnership RUBRIC
elseif (in_array($page_content, $rubric_Files)) {
  ob_start(); include ("Content/Pages/Projects/rubric/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/rubric-header.php");
}

// Youth Driven Space
elseif (in_array($page_content, $yds_Files)) {
  ob_start(); include ("Content/Pages/Projects/yds/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/yds-header.php");
}

// Strong Beginnings Program
elseif (in_array($page_content, $sbp_Files)) {
  ob_start(); include ("Content/Pages/Projects/sbp/$page_content.php"); $content = ob_get_clean();
  include("Views/Shared/Partials/sbp-header.php");
}

// ALL OTHER PAGES
else {
  ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean();
}

?>


<section class="container-fluid">
  <div class="container">
    <div class="row">
      <?php

      // 21st Century Community Learning Centers
      if (in_array($page_content, $tfcclc_Files)) {
        ob_start(); include ("Content/Pages/Projects/tfcclc/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/tfcclc-sidebar.php");
      }

      // Great Start Readiness Program
      elseif (in_array($page_content, $gsrp_Files)) {
        ob_start(); include ("Content/Pages/Projects/gsrp/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/gsrp-sidebar.php");
      }

      // Childcare Mapping Project
      elseif (in_array($page_content, $cmp_Files)) {
        ob_start(); include ("Content/Pages/Projects/cmp/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/cmp-sidebar.php");
      }

      // Youth-Adult Partnership RUBRIC
      elseif (in_array($page_content, $rubric_Files)) {
        ob_start(); include ("Content/Pages/Projects/rubric/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/rubric-sidebar.php");
      }

      // Youth Driven Space
      elseif (in_array($page_content, $yds_Files)) {
        ob_start(); include ("Content/Pages/Projects/yds/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/yds-sidebar.php");
      }

      // Strong Beginnings Program
      elseif (in_array($page_content, $sbp_Files)) {
        ob_start(); include ("Content/Pages/Projects/sbp/$page_content.php"); $content = ob_get_clean();
        include("Views/Shared/Partials/sbp-sidebar.php");
      }

      // ALL OTHER PAGES
      else {
        ob_start(); include ("Content/Pages/$page_content.php"); $content = ob_get_clean();
      }

      ?>


        <div class="<?php if (isset ($has_sidebar)){echo 'col-md-9';} else echo 'col-md-11'; ?> py-5">
          <?php echo $content; ?>
        </div>
    </div>
  </div>
</section>

<section class="project-categories">
  <div class="container">
    <div class="row">

      <div class="col-md project-category oos-time">
        <h2>Out-of-School Time</h2>

        <ul>
          <li>
            <a href="tfcclc-overview">21st Century Community Learning Centers <small>(21st CCLC)</small></a>
          </li>

          <li>
            <a href="yds-overview">Youth Driven Spaces</a>
          </li>

          <li>
            <a href="rubric-overview">Youth-Adult Partnership RUBRIC</a>
          </li>
        </ul>
      </div>

      <div class="col-md project-category ece">
        <h2>Early Childhood Education</h2>

        <ul>
          <li>
            <a href="gsrp-overview">Great Start Readiness Program (GSRP)</a>
          </li>

          <li>
            <a href="sbp-overview">Strong Beginnings Program</a>
          </li>

          <li>
            <a href="cmp-overview">Child Care Mapping Project</a>
          </li>
        </ul>
      </div>

    </div>
  </div>
</section>
